// Bài 1: Tính tiền lương nhân viên
/**
 * Input:
 * workdays=24;
 * salary=100000;
 *
 * Process:
 * salary=workdays*daysalary
 *
 * Output:
 * salary=?
 *
 *
 *
 *
 */

const daysalary = 100000;
var workdays = 24;
var salary = null;

salary = workdays * daysalary;
console.log("Bài tập 1: tính tiền lương, tiền lương bằng=", salary);

// -------------------------------------------------------------------------------
// Bài 2: Tính tính giá trị trung bình
/**
 * Input:
 *
 * real1=1
 * real2=2
 * real3=3
 * real4=4
 * real5=5
 *
 * Process:
 * average=(real1+real2+real3+real4+real5)/5
 *
 * Output:
 * average=?
 *
 */
var real1 = 1;
var real2 = 2;
var real3 = 3;
var real4 = 4;
var real5 = 5;
var average = null;

average = (real1 + real2 + real3 + real4 + real5) / 5;
console.log("giá trị trung bình của 5 số là: ", average);
// -------------------------------------------------------------------------------
// Bài 3: Quy đổi tiền
/**
 * Input:
 *USD=23500
 *VND=100
 *
 * Process:
 * US2VND=USD*VND
 *
 * Output:
 * US2VND=?
 *
 */
const USD = 23500;
var VND = 100;
var US2VND = null;

US2VND = USD * VND;
console.log("100 đô la mỹ tương đương ", US2VND, "VNĐ");
// -------------------------------------------------------------------------------
// Bài 4: Tính diện tích chu vi hình chữ nhật
/**
 * Input:
 *dai=10
 *rong=5
 *
 * Process:
 * dientich=dai*rong
 * chuvi=(dai+rong)*2
 * Output:
 * dientich=?
 * chivi=?
 *
 */

var dai = 10;
var rong = 5;
var chuvi = null;
var dientich = null;

dientich = dai * rong;
chuvi = (dai + rong) * 2;

console.log("Diện tích là:", dientich, "m");
console.log("Chu vi là", chuvi, "m");
// -------------------------------------------------------------------------------
// Bài 5: Tính tổng hai ký số
/**
 * Input:
 *num=12
 *
 *
 * Process:
 * donvi=num%10
 * chuc=num/10
 * tong=chuc+donvi
 * Output:
 * tong=?
 *
 */

var num = 12;

var chuc = null;
var donvi = null;

chuc = Math.floor(num / 10);
donvi = num % 10;
tong = chuc + donvi;

console.log("Tổng hai ký số là:", tong);
